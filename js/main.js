//DEFAULT VALUES

var cursorTopPos = 22; //half img height
var cursorLeftPos = 22; //half img width
var imageIDs = [];

var idGenerator = function() {
  return Math.random().toString(36).substr(2, 9);
};

var imageLoader = function(pIndex, pImgPath) {
  var id = idGenerator();
  var img = '<img class="scThemeIMG" src=./szablony/' + pImgPath + ' id=' + id + '>';
  imageIDs.push(id);
  $("#sc-themes").append(img);
  $('#' + id).draggable({
    containment: "#sc-main",
    helper: "clone",
    cursorAt: {
      top: cursorTopPos,
      left: cursorLeftPos
    },
    scroll: false
  });
};

var isNew = function(pId) {
  var i, max = imageIDs.length;
  for (i = 0; i < max; i++) {
    if (pId === imageIDs[i]) {
      return true;
    }
  }
  return false;
};

var isIn = function(pEvent) {
  var $droppableElem = $($("#sc-preview")[0]);
  var width = $droppableElem.width();
  var height = $droppableElem.height();
  var offsetTop = $droppableElem.offset().top;
  var offsetLeft = $droppableElem.offset().left;
  var pageX = pEvent.pageX;
  var pageY = pEvent.pageY;
  var minX = offsetLeft + cursorTopPos;
  var maxX = offsetLeft + width - cursorTopPos;
  var minY = offsetTop + cursorLeftPos;
  var maxY = offsetTop + height - cursorLeftPos;
  return (pageX >= minX && pageX <= maxX && pageY >= minY && pageY <= maxY);
};

//ColorPicker init-----
$('#sc-colorPicker').ColorPicker({
  flat: true,
  onSubmit: function(pHsb, pHex, pRgb, pEl) {
    var hex = '#' + pHex;
    $('#sc-preview').css({
      'background-color': hex
    });
    $("#sc-colorPicker").css({
      display: 'none'
    });
  }
});

//LOAD IMGs-------------------------------------------------------
$.each(window.PolishArtStyleScImgPaths, imageLoader);

//D&D SUPPORT-----------------------------------------------------
$("#sc-preview").droppable({
  drop: function(pEvent, pUI) {
    var draggedElem;
    if (isNew(pUI.draggable[0].id)) {
      draggedElem = $($(pUI.helper[0]).clone()[0]).removeClass();
    } else {
      draggedElem = $(pUI.draggable[0]).removeClass();
    }
    if (!isIn(pEvent)) {
      $(draggedElem).remove();
      return false;
    }
    $(draggedElem).css({
      top: pEvent.clientY - cursorTopPos,
      left: pEvent.clientX - cursorLeftPos,
      position: 'fixed'
    }).draggable({
      containment: "#sc-main",
      cursorAt: {
        top: cursorTopPos,
        left: cursorLeftPos
      },
      scroll: false
    });
    $(draggedElem).appendTo(this);
  }
});
$("#sc-main").droppable({
  drop: function(pEvent, pUI) {
    var draggedElem;
    if (isNew(pUI.draggable[0].id)) {
      draggedElem = $($(pUI.helper[0]).clone()[0]).removeClass();
    } else {
      draggedElem = $(pUI.draggable[0]).removeClass();
    }
    if (!isIn(pEvent)) {
      $(draggedElem).remove();
    }
  }
});

//FORM-------------------------------------------------
function validateForm() {
  var isImageUploaded = document.getElementById("sc-form_file_path").value;
  if (!isImageUploaded) {
    alert("Nie stworzono wzoru!");
  }
  return !!isImageUploaded;
}

//SHOW SCARF CREATOR-------------------------------------------------
$('#sc-form_file_chooser').click(function() {
  $("#sc-main").removeClass('fadeOut');
  $("#sc-main").addClass('fadeIn');
  $("#sc-main").css({
    display: "block"
  });
});

//SHOW/HIDE COLOR PICKER-------------------------------------------------
$('#sc-colorPickerBtn').click(function() {
  $("#sc-colorPicker").css({
    display: 'inline-flex'
  });
});

//SAVE DIV AS CANVAS-------------------------------------------------------
$("#sc-submitBtn").click(function() {
  var $droppableElem = $($("#sc-preview")[0]);
  html2canvas($droppableElem, {
    onrendered: function(canvas) {
      var $scarfPreview = $("#sc-scarf_preview");
      $scarfPreview.empty();
      $scarfPreview.append(canvas);
      $scarfPreview.addClass('mt20');
    },
    allowTaint: true
  });

  $("#sc-main").removeClass('fadeIn');
  $("#sc-main").addClass('fadeOut');
  setTimeout(function(){
    $("#sc-main").css({
      display: "none"
    });
  }, 500);
  document.getElementById("sc-form_file_path").value = "Edytuj wzór...";
});
